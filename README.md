# Common Gitlab CI templates

To enable all features:

```yaml
include:
  - project: "html-validate/gitlab-ci"
    file: "/default.yml"

default:
  image: node:latest

stages:
  - prepare
  - test
  - build
  - release
```

## Build

Executes `npm run build` followed by linting the NPM package with `npm-pkg-lint`.

```yaml
include:
  - project: "html-validate/gitlab-ci"
    file: "/templates/build.yml"
```

Expects `build` command to:

- Compile files to `dist/`

## Tests

Executes `npm test`

```yaml
include:
  - project: "html-validate/gitlab-ci"
    file: "/templates/lint.yml"
```

Expects test command to:

- Output branch coverage on stdout
- Create a junit-compatible report in `test/jest.xml`
- Create HTML coverage report in `coverage/`

## Linting and static analysis

```yaml
include:
  - project: "html-validate/gitlab-ci"
    file: "/templates/lint.yml"
```

Supports the following linting and static analysis:

### ESLint

- Command: `npx eslint --max-warnings 0`.
- Condition: `.eslintrc.js` or `.eslintrc.json`

### Prettier

- Command `npx prettier --check .`
